let numberColors = 6;
let colors = [];
let pickedColor;

let squares = document.querySelectorAll(".square");
let rgbColorDisplay = document.querySelector("#rgbColorDisplay");
let message = document.querySelector("#message");
let heading = document.querySelector("h1");
let resetButton = document.querySelector("#reset");
let mode = document.querySelectorAll(".mode");

init();

resetButton.addEventListener("click", function(){
	reset();
});


function init() {
	setUpMode();
	setUpSquares();
	reset();
}

function setUpMode() {
	for (let i = 0; i < mode.length; i++) {
		mode[i].addEventListener("click", function() {

			//remove class "selected" from all
			mode[0].classList.remove("selected");
			mode[1].classList.remove("selected");

			//add class "selected" only to current
			this.classList.add("selected");

			if(this.textContent === "Easy"){
				numberColors = 3;
			} else {
				numberColors = 6;
			}
			reset();
		});
	}
}

function setUpSquares() {
	for (let i = 0; i < squares.length; i++) {
		squares[i].addEventListener("click", function() {
			let clickedColor = this.style.backgroundColor;

			if (clickedColor === pickedColor) {
				message.textContent = "Correct!";
				heading.style.backgroundColor = pickedColor;
				resetButton.textContent = "Play again?";
				correctColor(pickedColor);
			} else {
				this.style.backgroundColor = "#232323";
				message.textContent = "Try again!";
			}
		});
	}
}

//Reset the game
function reset(){
	colors = generateRandomColors(numberColors);
	pickedColor = pickRandomColorFromArray();
	rgbColorDisplay.textContent = pickedColor;

	for(let i = 0; i < squares.length; i++){
		if(colors[i]){
			squares[i].style.display = "block";
			squares[i].style.backgroundColor = colors[i];
		} else {
			squares[i].style.display = "none";
		}
	}

	heading.style.backgroundColor = "steelblue";
	resetButton.textContent = "New colors";
	message.textContent = " ";
}


function getRandomColor(){
	let r = Math.floor(Math.random() * 256);
	let g = Math.floor(Math.random() * 256);
	let b = Math.floor(Math.random() * 256);

	return "rgb(" + r + ", " + g + ", " + b + ")";
}


function generateRandomColors(count){
	let result = [];
	for(let i = 0; i < count; i++){
		result.push(getRandomColor());
	}
	return result;
}


function pickRandomColorFromArray(){
	let random = Math.floor(Math.random() * colors.length);
	return colors[random];
}

//Make all squares in the correct color.
function correctColor(color){
	for(let i = 0; i < squares.length; i++){
		squares[i].style.backgroundColor = color;
	}
}